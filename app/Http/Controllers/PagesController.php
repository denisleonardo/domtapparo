<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Variations;
use App\SubCategories;
use App\Products;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Banners;

class PagesController extends Controller
{
    public $lang;

    public function __construct()
    { }

    public function home()
    {
        $lang = \Lang::getLocale();

        //Seleciona manualmente a categoria principal
        $category = Categories::where('lang', '=', $lang)->where('principal', '<>', '1')->get();
        $line = Categories::where('principal', '=', '1')->where('lang', '=', $lang)->get();


        //Selecionar todos os banner

        $banners = Banners::where('lang', '=', $lang)->get();

        return view('home', ['banners' => $banners, 'category' => $category, 'line' => $line]);

    }
    
    //Listagem de produtos da subcategorias
    public function subcategories_products(Request $req)
    {
        $url = $req->url;

        $categoria = Categories::where('url', '=', $url)->get()->first();
        $items = $categoria->products()->paginate(12);

        return view('category'
        , 
         [
            'categoria' => $categoria,
            'items' => $items
         ]
    );
    }

    public function product(Request $req)
    {
        $url = $req->url;
        $product = Products::where('url', '=', $url)->get()->first();
        $categoria = $product->categories()->get()->first();
        $item = $product->categories()->get()->first();
        $variations = $product->variations()->get();

        $data[] = array(
            "nome" => "products.material",
            "valor" => $product->material
        );

        $data[] = array(
            "nome" => "products.espessura",
            "valor" => $product->espessura
        );

        $data[] = array(
            "nome" => "products.rev_interno",
            "valor" => $product->rev_interno
        );

        $data[] = array(
            "nome" => "products.rev_externo",
            "valor" => $product->rev_externo
        );

        $data[] = array(
            "nome" => "products.tampa",
            "valor" => $product->tampa
        );

        $data[] = array(
            "nome" => "products.vaporizador",
            "valor" => $product->vaporizador
        );

        $data[] = array(
            "nome" => "products.holder",
            "valor" => $product->holder
        );

        $data[] = array(
            "nome" => "products.pomeis",
            "valor" => $product->pomeis
        );

        $data[] = array(
            "nome" => "products.alca",
            "valor" => $product->alca
        );

        $data[] = array(
            "nome" => "products.agulheto",
            "valor" => $product->agulheto
        );

        return view('products'
        , [
            'product' => $product,
            'variations' => $variations,
            'categoria' => $categoria,
            'descriptions' => $data,
            '_subtitle' => $product->name
        ]
    );
    }

    public function search(Request $req)
    {
        $lang = \Lang::getLocale();
        $term = $req->query('s');

        $products = Products::whereHas('Variations',function($q) use ($term,$lang){
            $q->where('lang', '=', $lang)
            ->where(function ($query) use ($term) {
                $query
                    ->orWhere('sku','=',$term);
            });
        })->orWhere('name', 'like', '%' . $term . '%')->paginate(12);


        return view('search', [
            'products' => $products
        ]);
    }

    public function email(Request $req)
    {
        echo 'test';
        // $form = $req->all();
        // Mail::to('digital@agencialed.com.br')
        //     ->send(new SendMail($form));

        // if (Mail::failures()) {
        //     echo '0';
        //     exit;
        // }
        // echo '1';
    }

    public function ajaxRequest()

    {

        return view('ajaxRequest');

    }

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function ajaxRequestPost()

    {

        $input = request()->all();

        return response()->json(['success'=>'Got Simple Ajax Request.']);

    }

}
