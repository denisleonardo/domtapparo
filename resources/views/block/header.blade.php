<header>
    <div class="banner text-center">
        <img class="logo " src="{{ asset('img/Dom-Tápparo-Logo-preto-e1535456991751.png') }}">
    </div>
    <nav class="navbar navbar-expand-lg color-text" style="background: #cbb36d;">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation" style="color: #fff;">
                <span class="navbar-toggler-icon" ><img src="{{ asset('img/menu-icon.png') }}"></span>
                </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                    <a class="nav-link" href="{{ route('home', ['locale'=>Config::get('app.locale')]) }}">@lang('site.home') <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('distribuidor', ['locale'=>Config::get('app.locale')]) }}">@lang('site.be')</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @lang('site.es')
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">@lang('site.en')</a>
                            <a class="dropdown-item" href="#">@lang('site.es')</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://api.whatsapp.com/send?1=pt_BR&phone=5517996571818">@lang('distribuitor.contact')</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="@lang('site.search')" aria-label="@lang('site.search')">
                    <button class="btn text-white my-2 my-sm-0" type="submit">@lang('site.search')</button>
                </form>
            </div>
        </div>
    </nav>
</header>