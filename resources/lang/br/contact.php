<?php

return [
    'title'=>'Contato',
    'name' => 'Nome',
    'email' => 'E-mail',
    'company' => 'Empresa',
    'subject' => 'Assunto',
    'message' => 'Mensagem',
    'send'=>'Enviar'
];
