@extends('layouts.default')
@section('content')
<section>
        <div class="container">
            <div class="nav-head">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home', ['locale'=>Config::get('app.locale')]) }}">@lang('site.home')</a></li>
                        <li class="breadcrumb-item active">{{ $categoria->nome }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>
<section>
    <div class="container">        
        <h5 class="mb-3" style="font-size: 22px;">{{$categoria->nome}}</h5>
        <div class="results">
            <h6 style="font-size: 14px;">
                @lang('category.result') {{$items->count()}} @lang('site.res')
            </h6>
        </div>        
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            @foreach ($items as $item)
            <div class="imgBox col-lg-3 col-6">
                <a class="prod-link text-center" href="{{ route('products',  ['locale'=>Config::get('app.locale'), 'url'=>$item->url]) }}">
                    @if ($item->image == "")
                    <img src="{{ asset('img/no-image.webp') }}" alt="" class="img-fluid">
                @else
                    <img src="{{ asset('img/produtos/'.$item->image) }}" alt="" class="img-fluid">
                @endif

                    <h6 class="text-uppercase">{{$item->name}}</h6>
                </a>               
                <div class="button text-center">
                    <a href="{{ route('products',  ['locale'=>Config::get('app.locale'), 'url'=>$item->url]) }}"><button class="btn-rm mb-5">@lang('site.read')</button></a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="text-xs-center text-center">
            {{ $items->links() }}
        </div>
    </div>
</section>
@endsection