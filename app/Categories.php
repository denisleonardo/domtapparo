<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = "categories";
    //protected $fillable = ['*'];
    protected $guarded = [];
    public $timestamps = false;

    public function products(){
        return $this->belongsToMany('App\Products','categorias_has_products','categorias_id','products_id');
    }
}
