@extends('layouts.default')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div id="slider" class="nivoSlider">
            <img src="{{ asset('img/banner_espanhol_2_1920x822_alt1_aprov.png')}}">
            <img src="{{ asset('img/banner_espanhol_3_1920x822_alt2_aprov.png')}}">
            <img src="{{ asset('img/Banner_4_esp.jpg')}}">
        </div>
    </div>
</div>
<section class="tipo-bebida my-5">
    <div class="container">
        <div class="row d-flex justify-content-center">
         @foreach ($line as $item)
            <div class="image1 col-md-6 col-sm-6 col-12">
                <a href="{{ route('category', ['locale'=>Config::get('app.locale'), 'url'=>$item->url])}}">
                    <img class="img-fluid" src="{{asset('img/bnt2.png')}}">
                </a>
            </div>
            <div class="image2 col-md-6 col-sm-6 col-12">
                <a href="{{ route('category', ['locale'=>Config::get('app.locale'), 'url'=>$item->url])}}">
                    <img class="img-fluid" src="{{asset('img/btn1.png')}}">
                </a>
            </div>
        @endforeach
        </div>
    </div>
</section>
<section class="cat-bg pb-5">
    <div class="text-center">
        <div class="categoria pt-5">
            <h2 id="cat-title">Categories</h2>
        </div>
    </div>
    <div class="container-fluid my-2">
        <div class="row d-flex justify-content-center">
            <div class="col-sm-2 col-12 my-2">
                <img class="imgLogo" src="{{asset('img/imagem2.png')}}">
            </div>
        </div>
    </div>
    <div class="container text-center ">
        <div class="row mt-4 mb-4">
            @foreach ($category as $item)
            <div class="col-4 mb-3">
                    <div class="cat cool-link">
                    <a href="{{ route('category', ['locale'=>Config::get('app.locale'), 'url'=>$item->url]) }}" class="cat-tip">
                            <div class="p-3 h4 mt-2">{{ $item->nome }}</div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endsection