<?php

return [
    'be' => 'Ser una distribución',
    'by'  => 'Al convertirse en un distribuidor de Dom Tápparo, tiene las ventajas y fortalezas de una marca tradicional y galardonada. Estas son algunas de las grandes razones para convertirse en nuestro distribuidor:',
    '40' => '40 años en el mercado brasile.',
    'products' => 'Productos de fabricación propia.',
    'presente' => 'Presente en los principales eventos nacionales e internacionales.',
    '2gold' => '2 medallas de oro en la Feria Nacional Brasileña de Cachaça.',
    '2conse' => '2 medallas de oro consecutivas en el Concours Mondial de Bruxelles - edición de Brasil.',
    'reco' => 'Reconocido en territorio nacional e internacional.',
    'ex' => 'Departamento de exportación',
    'form' => 'Para saber más sobre Dom Tápparo y cómo convertirse en distribuidor, complete el formulario.',
    'name' => 'Salón DomTápparo',
    'nform' => 'Nombre',
    'eform' => 'Correo electronico',
    'cform' => 'Nombre de la empresa',
    'sform' => 'Tema',
    'mform' => 'Mensaje',
    'contact' => 'Contáctenos',
];


