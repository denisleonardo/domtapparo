<?php

return [
    'raiz-title' => 'Export Departament',
    'title' => 'Be our distributor',
    'text' => '
    <p>
    Considered one of the largest companies of the segment in the country, Eirilar has an industrial complex of approximately 93,000 square meters. The facility located on Km 474.5 of Euclides da Cunha Highway, in Tanabi, state of São Paulo, is responsible for the production, development and logistics of its products.
    </p>
    <p>
    Founded in 1977, the aluminum industry Eirilar is constantly working on the research of new technologies incorporated into the production process, mainly to become systematically more efficient and environmental friendlier.
    </p>
        <h5>We currently have more than 15 thousand points of sales in Brazil and we are expanding our business towards the international market.</h5>
    <p>
            Distributors of Eirilar products collect count:
        <ul>
            <li>Diversified product line</li>
            <li>Marketing support to boost sales</li>
            <li>Possibility of labeling for the international market</li>
            <li>Exclusivity contracts in the destination of marketing</li>
            <li>Specialized foreign trade support</li>
        </ul>
    </p>
    <p></p>
    <p>
        <strong>Phone:</strong> 0800 707 9220<br />
        <strong>E-mail:</strong> <a href="mailto:vendas@eirilar.com.br">vendas@eirilar.com.br</a>
    </p>
    ',
    'company' => 'Company',
    'subject' => 'Subject',
    'message' => 'Message',
    'send' => 'Send'
];
