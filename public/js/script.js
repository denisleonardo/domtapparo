$(window).on('load', function() {
    $('#slider').nivoSlider({
        directionNav: true,
        controlNav: false,
        directionNav: true,
        pauseOnHover: true,
        animSpeed: 500,
        pauseTime: 3000,
        prevText: '<h1 style="font-size: 30px; padding-left: 30px;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></h1>',
        nextText: '<h1 style="font-size: 30px; padding-right: 30px;"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h1>',
    });
});