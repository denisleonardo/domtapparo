<?php

return [
    'title'=>'Contact us',
    'name' => 'Name',
    'email' => 'E-mail',
    'company' => 'Company',
    'subject' => 'Subject',
    'message' => 'Message',
    'send'=>'Send'
];
