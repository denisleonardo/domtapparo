<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variations extends Model
{
    protected $table = "variations";
    //protected $fillable = [];
    protected $guarded = [];
    public $timestamps = false;

    public function products(){
        return $this->hasOne('App\Products','id');
    }

}
