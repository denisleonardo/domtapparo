<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";
    //protected $fillable = ['*'];
    protected $guarded = [];
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany('App\Categories', 'categorias_has_products', 'products_id', 'categorias_id');
    }

    public function variations()
    {
        return $this->hasMany('App\Variations', 'products_id');
    }

}
